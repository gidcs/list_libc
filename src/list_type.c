#include <stdio.h>
#include "list_type.h"

int list_print_item(LIST_TTYPE *item, void *params){
    printf("(%d,%d) ", item->first, item->second);
    return 0;
}
