#ifndef LIST_HEADER
#define LIST_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list_type.h"

#define DEBUG_LIST
#ifdef DEBUG_LIST
    #define __LDEBUG(fmt, args...)    fprintf(stderr, "    [list] "fmt, ## args)
#else
    #define __LDEBUG(fmt, args...)    /* Don't do anything in release builds */
#endif

#define LIST_ALLOC_FAIL_CNT 5

#ifdef DEBUG_LIST
    #define LIST_ALLOC_FAIL_SIM
#endif

typedef struct node_tt Node;

struct node_tt {
    LIST_TTYPE val;
    Node *prev;
    Node *next;
};

typedef struct list_tt {
    Node *front;
    Node *back;
    int size;
#ifdef LIST_ALLOC_FAIL_SIM
    int alloc_cnt;
#endif
} List;

void list_init(List *list);
int list_size(List *list);
Node *list_front(List *list);
Node *list_back(List *list);
void list_free(List *list);
Node *list_alloc(List *list, LIST_TTYPE item);
int list_push_front(List *list, LIST_TTYPE item);
int list_push_back(List *list, LIST_TTYPE item);
int list_pop_front(List *list, LIST_TTYPE *item);
int list_pop_back(List *list, LIST_TTYPE *item);
void list_do_all(List *list, int (*fptr)(LIST_TTYPE *, void *), void *params);
void list_rdo_all(List *list, int (*fptr)(LIST_TTYPE *, void *), void *params);
void list_merge(List *first_list, List *second_list);
int list_copy(List *dest_list, List *src_list);

#endif /* !LIST_HEADER */
