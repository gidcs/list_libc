#include "list.h"

void list_init(List *list){
    list->front = NULL;
    list->back = NULL;
    list->size = 0;
#ifdef LIST_ALLOC_FAIL_SIM
    list->alloc_cnt = 0;
#endif
    __LDEBUG("%s(%p)\n", __func__, list);
}

int list_size(List *list){
    __LDEBUG("%s(%p)\n", __func__, list);
    return list->size;
}

Node *list_front(List *list){
    __LDEBUG("%s(%p)\n", __func__, list);
    return list->front;
}

Node *list_back(List *list){
    __LDEBUG("%s(%p)\n", __func__, list);
    return list->back;
}

void list_free(List *list){
    Node *ptr, *tmp;
    ptr = list->front;
    
    while(NULL != ptr){
        tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }
    __LDEBUG("%s(%p)\n", __func__, list);
}

Node *list_alloc(List *list, LIST_TTYPE item){
    Node *tmp;
    tmp = NULL;

#ifdef LIST_ALLOC_FAIL_SIM
    list->alloc_cnt++;
    if(LIST_ALLOC_FAIL_CNT == list->alloc_cnt){
        list->alloc_cnt--;
        __LDEBUG("%s(%p) alloc failed (fake fail)\n", __func__, list);
        return NULL;
    }
#endif
    
    tmp = malloc(sizeof(Node));
    if(NULL == tmp){
        return NULL;
    }
    memcpy(&(tmp->val), &item, sizeof(LIST_TTYPE));
    tmp->next = NULL;
    tmp->prev = NULL;
    __LDEBUG("%s(%p) alloc ok\n", __func__, list);
    return tmp;
}

int list_push_front(List *list, LIST_TTYPE item){
    Node *tmp, *tmp_front;
    
    tmp = list_alloc(list, item);
    if(NULL == tmp){
        __LDEBUG("%s(%p) list alloc failed\n", __func__, list);
        return -1;
    }
    
    tmp_front = list_front(list);
    if(NULL == tmp_front){ //no item in list
        list->front = tmp;
        list->back = tmp;
    }
    else{
        tmp->next = tmp_front;
        tmp_front->prev = tmp;
        list->front = tmp;
    }
    
    list->size++;
    __LDEBUG("%s(%p) size=%d\n", __func__, list, list->size);
    return list->size;
}

int list_push_back(List *list, LIST_TTYPE item){
    Node *tmp, *tmp_back;
    
    tmp = list_alloc(list, item);
    if(NULL == tmp){
        __LDEBUG("%s(%p) list alloc failed\n", __func__, list);
        return -1;
    }
    
    tmp_back = list_back(list);
    if(NULL == tmp_back){ //no item in list
        list->front = tmp;
        list->back = tmp;
    }
    else{
        tmp_back->next = tmp;
        tmp->prev = tmp_back;
        list->back = tmp;
    }
    
    list->size++;
    __LDEBUG("%s(%p) size=%d\n", __func__, list, list->size);
    return list->size;
}

int list_pop_front(List *list, LIST_TTYPE *item){
    Node *tmp_front, *tmp_next;
    
    tmp_front = list_front(list);
    if(NULL == tmp_front){ //no item in list
        return -1;
    }
    else{
        memcpy(item, &(tmp_front->val), sizeof(LIST_TTYPE));
        tmp_next = tmp_front->next;
        if(NULL != tmp_next){
            tmp_next->prev = NULL;
            list->front = tmp_next;
        }
        else{ //last one in list
            list->front = NULL;
            list->back = NULL;
        }
        free(tmp_front);
    }
    
    list->size--;
    __LDEBUG("%s(%p) size=%d\n", __func__, list, list->size);
    return list->size;
}

int list_pop_back(List *list, LIST_TTYPE *item){
    Node *tmp_back, *tmp_prev;
    
    tmp_back = list_back(list);
    if(NULL == tmp_back){ //no item in list
        return -1;
    }
    else{
        memcpy(item, &(tmp_back->val), sizeof(LIST_TTYPE));
        tmp_prev = tmp_back->prev;
        if(NULL != tmp_prev){
            tmp_prev->next = NULL;
            list->back = tmp_prev;
        }
        else{
            list->front = NULL;
            list->back = NULL;
        }
        free(tmp_back);
    }
    
    list->size--;
    __LDEBUG("%s(%p) size=%d\n", __func__, list, list->size);
    return list->size;
}

void list_do_all(List *list, int (*fptr)(LIST_TTYPE *, void *), void *params){
    Node *ptr;
    int break_flag;
    
    ptr=list_front(list);
    __LDEBUG("%s(%p) list: ", __func__, list);
    while(ptr){
        break_flag = (*fptr)(&(ptr->val), params);
        if(1 == break_flag) break;
        ptr = ptr->next;
    }
}

void list_rdo_all(List *list, int (*fptr)(LIST_TTYPE *, void *), void *params){
    Node *ptr;
    int break_flag;
    
    ptr=list_back(list);
    __LDEBUG("%s(%p) list: ", __func__, list);
    while(ptr){
        break_flag = (*fptr)(&(ptr->val), params);
        if(1 == break_flag) break;
        ptr = ptr->prev;
    }
}

void list_merge(List *first_list, List *second_list){
    Node *first_back, *second_front;

    first_back=list_back(first_list);
    second_front=list_front(second_list);
    if(NULL == first_back && NULL == second_front){ //both empty
        //do nothing
    }
    else if(NULL == first_back && NULL != second_front){ //first_list is empty
        //assign attr of second to first
        first_list->front = second_list->front;
        first_list->back = second_list->back;
        first_list->size = second_list->size;
        //reset second
        list_init(second_list);
    }
    else if(NULL != first_back && NULL == second_front){ //second_list is empty
        //do nothing
    }
    else{ //both nonempty
        //merge list of second to first
        first_back->next = second_front;
        second_front->prev = first_back;
        first_list->back = second_list->back;
        first_list->size += second_list->size;
        //reset second
        list_init(second_list);
    }

    __LDEBUG("%s(%p, %p) first_size=%d\n", __func__, 
            first_list, second_list, first_list->size);
}

int list_copy(List *dest_list, List *src_list){
    Node *ptr;
    int ret;
    
    list_init(dest_list);
    
    ptr=list_front(src_list);
    while(ptr){
        ret = list_push_back(dest_list, ptr->val);
        if(ret==-1){
            break;
        }
        ptr = ptr->next;
    }
    __LDEBUG("%s(%p, %p) dest_size=%d\n", __func__, 
            dest_list, src_list, dest_list->size);

    if(ret==-1) return -1;
    else return 0;
}
