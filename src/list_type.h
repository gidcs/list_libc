typedef struct tuple_tt {
    int first;
    int second;
} tuple_t;

#define LIST_TTYPE tuple_t

int list_print_item(LIST_TTYPE *item, void *params);
