#include <stdio.h>
#include "src/list.h"

#define DEBUG_MAIN
#ifdef DEBUG_MAIN
    #define __MDEBUG(fmt, args...)    fprintf(stderr, "[main] "fmt, ## args)
#else
    #define __MDEBUG(fmt, args...)    /* Don't do anything in release builds */
#endif

#define MAX 10

int main(int argc, char *argv[]){
    LIST_TTYPE item;
    int i, j;
    int ret;
    List list[5];

    __MDEBUG("list0(%p)/list1(%p) init\n", &(list[0]), &(list[1]));
    list_init(&(list[0]));
    list_init(&(list[1]));
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    for(j = 0; j < 2; j++){
        __MDEBUG("list%d(%p) push back/front\n", j, &(list[j]));
        for(i = 0; i < MAX+1; i++){
            item.first=i*j+1;
            item.second=i+j*2;
        
            if(i%2==0)
                ret = list_push_back(&(list[j]), item);
            else
                ret = list_push_front(&(list[j]), item);
            if(-1 == ret){
                __MDEBUG(" push i=%d item='reject' ret=%d\n", i, ret);
                break;
            }
            else{
                __MDEBUG(" push i=%d item=(%d,%d) ret=%d\n", i, item.first, item.second, ret);
                list_do_all(&(list[j]), &list_print_item, NULL); 
                printf("\n");
                list_rdo_all(&(list[j]), &list_print_item, NULL);
                printf("\n");
            }
        }
        __MDEBUG("---\n");
    }

    __MDEBUG("---\n");
    __MDEBUG("copy all item in list0(%p) and construct list2(%p)\n", &(list[0]), &(list[2]));
    list_copy(&(list[2]), &(list[0]));
    list_do_all(&(list[2]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[2]), &list_print_item, NULL);
    printf("\n");

    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("merge list0(%p) and list1(%p), list1 will become empty list\n", &(list[0]), &(list[1]));
    list_merge(&(list[0]), &(list[1]));
    list_do_all(&(list[0]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[0]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[1]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[1]), &list_print_item, NULL);
    printf("\n");

    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("init list3(%p)/list4(%p)\n", &(list[3]), &(list[4]));
    list_init(&(list[3]));
    list_init(&(list[4]));
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("copy empty list3(%p) to list4(%p)\n", &(list[3]), &(list[4]));
    list_copy(&(list[4]), &(list[3]));
    list_do_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("merge empty list3(%p) and empty list4(%p)\n", &(list[3]), &(list[4]));
    list_merge(&(list[3]), &(list[4]));
    list_do_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("copy list0(%p) to list3(%p)\n", &(list[0]), &(list[3]));
    list_copy(&(list[3]), &(list[0]));
    list_do_all(&(list[0]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[0]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("merge list3(%p) and empty list4(%p)\n", &(list[3]), &(list[4]));
    list_merge(&(list[3]), &(list[4]));
    list_do_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[4]), &list_print_item, NULL);
    printf("\n");

    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("merge empty list4(%p) and list3(%p), list3 will become empty\n", &(list[4]), &(list[3]));
    list_merge(&(list[4]), &(list[3]));
    list_do_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[4]), &list_print_item, NULL);
    printf("\n");
    list_do_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    list_rdo_all(&(list[3]), &list_print_item, NULL);
    printf("\n");
    
    __MDEBUG("---\n");
    __MDEBUG("---\n");
    __MDEBUG("free list3(%p)/list4(%p)\n", &(list[3]), &(list[4]));
    list_free(&(list[3]));
    list_free(&(list[4]));

    __MDEBUG("---\n");
    __MDEBUG("---\n");
    for(j = 0; j < 3; j++){
        __MDEBUG("list%d(%p) push back/front\n", j, &(list[j]));
        list_do_all(&(list[j]), &list_print_item, NULL);
        printf("\n");
        list_rdo_all(&(list[j]), &list_print_item, NULL);
        printf("\n");
        for(i = 0; i<= MAX; i++){
            if(i%2==0)
                ret = list_pop_back(&(list[j]), &item);
            else
                ret = list_pop_front(&(list[j]), &item);
            if(-1 == ret){
                __MDEBUG(" pop i=%d item='empty' ret=%d\n", i, ret);
                break;
            }
            else{
                __MDEBUG(" pop i=%d item=(%d,%d) ret=%d\n", i, item.first, item.second, ret);
                list_do_all(&(list[j]), &list_print_item, NULL);
                printf("\n");
                list_rdo_all(&(list[j]), &list_print_item, NULL);
                printf("\n");
            }
        }
        list_free(&(list[j]));
        __MDEBUG("---\n");
    }
    return 0;
}

