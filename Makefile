
CC	= gcc
CXX	= g++ -std=c++11
CFLAGS = -Wall -g

# PROGS = netstat
PROGS = main
OBJ = main.o src/list.o src/list_type.o

all: $(PROGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.cpp
	$(CXX) $(CFLAGS) -c -o $@ $<

$(PROGS): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $@

clean:
	rm -f *~ $(PROGS) $(OBJ)
